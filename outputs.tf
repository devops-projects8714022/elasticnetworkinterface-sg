output "security_group_id" {
  description = "The ID of the security group"
  value       = aws_security_group.this.id
}

output "network_interface_id" {
  description = "The ID of the network interface"
  value       = aws_network_interface.this.id
}

output "network_interface_private_ips" {
  description = "The private IP addresses assigned to the network interface"
  value       = aws_network_interface.this.private_ips
}
