resource "aws_security_group" "this" {
  name_prefix = var.sg_name_prefix
  description = var.sg_description
  vpc_id      = var.vpc_id

  ingress {
    from_port   = var.ingress_from_port
    to_port     = var.ingress_to_port
    protocol    = var.ingress_protocol
    cidr_blocks = var.ingress_cidr_blocks
  }

  egress {
    from_port   = var.egress_from_port
    to_port     = var.egress_to_port
    protocol    = var.egress_protocol
    cidr_blocks = var.egress_cidr_blocks
  }

  tags = var.sg_tags
}

resource "aws_network_interface" "this" {
  subnet_id       = var.subnet_id
  private_ips     = var.private_ips
  security_groups = [aws_security_group.this.id]

  tags = var.eni_tags
}
