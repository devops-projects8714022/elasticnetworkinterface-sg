variable "sg_name_prefix" {
  description = "Prefix for the security group name"
  type        = string
}

variable "sg_description" {
  description = "Description for the security group"
  type        = string
}

variable "vpc_id" {
  description = "VPC ID where the security group will be created"
  type        = string
}

variable "ingress_from_port" {
  description = "Start port for ingress rule"
  type        = number
}

variable "ingress_to_port" {
  description = "End port for ingress rule"
  type        = number
}

variable "ingress_protocol" {
  description = "Protocol for ingress rule"
  type        = string
}

variable "ingress_cidr_blocks" {
  description = "CIDR blocks for ingress rule"
  type        = list(string)
}

variable "egress_from_port" {
  description = "Start port for egress rule"
  type        = number
}

variable "egress_to_port" {
  description = "End port for egress rule"
  type        = number
}

variable "egress_protocol" {
  description = "Protocol for egress rule"
  type        = string
}

variable "egress_cidr_blocks" {
  description = "CIDR blocks for egress rule"
  type        = list(string)
}

variable "sg_tags" {
  description = "Tags for the security group"
  type        = map(string)
  default     = {}
}

variable "subnet_id" {
  description = "Subnet ID where the ENI will be created"
  type        = string
}

variable "private_ips" {
  description = "List of private IP addresses to assign to the ENI"
  type        = list(string)
  default     = []
}

variable "eni_tags" {
  description = "Tags for the ENI"
  type        = map(string)
  default     = {}
}
