# elasticNetworkInterface-SG



## Getting started

```markdown
# Модуль Terraform для AWS Elastic Network Interface и Security Group

Этот модуль Terraform создает AWS Elastic Network Interface (ENI) и ассоциирует его с группой безопасности (Security Group).

## Использование

Чтобы использовать этот модуль, добавьте его в вашу конфигурацию Terraform следующим образом:

```hcl
module "eni_sg" {
  source = "./terraform-modules/eni-sg"

  sg_name_prefix      = "my-sg"
  sg_description      = "My security group"
  vpc_id              = "vpc-12345678"
  ingress_from_port   = 80
  ingress_to_port     = 80
  ingress_protocol    = "tcp"
  ingress_cidr_blocks = ["0.0.0.0/0"]
  egress_from_port    = 0
  egress_to_port      = 0
  egress_protocol     = "-1"
  egress_cidr_blocks  = ["0.0.0.0/0"]
  sg_tags             = { "Name" = "my-sg" }

  subnet_id   = "subnet-12345678"
  private_ips = ["10.0.0.10"]
  eni_tags    = { "Name" = "my-eni" }
}
```

## Переменные

Следующие переменные поддерживаются модулем:

### Переменные Security Group

- **`sg_name_prefix`** (string, обязательно): Префикс для имени группы безопасности.
- **`sg_description`** (string, обязательно): Описание для группы безопасности.
- **`vpc_id`** (string, обязательно): ID VPC, в котором будет создана группа безопасности.
- **`ingress_from_port`** (number, обязательно): Начальный порт для правила входящего трафика.
- **`ingress_to_port`** (number, обязательно): Конечный порт для правила входящего трафика.
- **`ingress_protocol`** (string, обязательно): Протокол для правила входящего трафика.
- **`ingress_cidr_blocks`** (list(string), обязательно): CIDR блоки для правила входящего трафика.
- **`egress_from_port`** (number, обязательно): Начальный порт для правила исходящего трафика.
- **`egress_to_port`** (number, обязательно): Конечный порт для правила исходящего трафика.
- **`egress_protocol`** (string, обязательно): Протокол для правила исходящего трафика.
- **`egress_cidr_blocks`** (list(string), обязательно): CIDR блоки для правила исходящего трафика.
- **`sg_tags`** (map(string), необязательно, по умолчанию = `{}`): Теги для группы безопасности.

### Переменные ENI

- **`subnet_id`** (string, обязательно): ID подсети, в которой будет создан ENI.
- **`private_ips`** (list(string), необязательно, по умолчанию = `[]`): Список частных IP-адресов, назначаемых ENI.
- **`eni_tags`** (map(string), необязательно, по умолчанию = `{}`): Теги для ENI.

## Выходные значения

Следующие выходные значения экспортируются:

- **`security_group_id`**: ID группы безопасности.
- **`network_interface_id`**: ID сетевого интерфейса.
- **`network_interface_private_ips`**: Частные IP-адреса, назначенные сетевому интерфейсу.

## Пример

```hcl
module "eni_sg" {
  source = "./terraform-modules/eni-sg"

  sg_name_prefix      = "my-sg"
  sg_description      = "My security group"
  vpc_id              = "vpc-12345678"
  ingress_from_port   = 80
  ingress_to_port     = 80
  ingress_protocol    = "tcp"
  ingress_cidr_blocks = ["0.0.0.0/0"]
  egress_from_port    = 0
  egress_to_port      = 0
  egress_protocol     = "-1"
  egress_cidr_blocks  = ["0.0.0.0/0"]
  sg_tags             = { "Name" = "my-sg" }

  subnet_id   = "subnet-12345678"
  private_ips = ["10.0.0.10"]
  eni_tags    = { "Name" = "my-eni" }
}
```